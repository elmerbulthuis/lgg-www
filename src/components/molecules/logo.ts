import { css, html } from "lit";
import { ComponentBase } from "../base.js";

export class LogoComponent extends ComponentBase {
    static styles = [...super.styles, css`
:host {
    padding: var(--padding);
}
img {
    width: 100%;
}
`]

    render() {
        return html`<img src="/lgg-logo-text-400.png" alt="Latency.GG eagle" />`;
    }
}
