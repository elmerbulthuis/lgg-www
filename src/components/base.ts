import { css, LitElement } from "lit";

export class ComponentBase extends LitElement {
    static styles = [css`

:host {
    font-family: sans-serif;
    font-size: var(--font-size);
}

h1,
h2,
h3,
p {
    all: initial;
    font: inherit;
    display: block;
    padding: var(--padding);
}
h1,
h2,
h3 {
    font-family:  sans-serif;
    color: var(--dark-color);
}
p {
    color: var(--dark-color);
}
h1 {
    font-size: 2em;
}
h2 {
    font-size: 1.5em;
}
h3 {
    font-size: 1.2em;
}

p {
    font-size: 1em;
}

pre {
    font-family: monospace;
    font-size: var(--font-size);
    padding: var(--padding);
}

dl {
  all: initial;
  font: inherit;
  display: block;
  padding: var(--padding);
}

`];

}
