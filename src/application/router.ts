import { Router } from "goodrouter";

export const router = createRouter();

function createRouter() {
    const router = new Router();

    router.insertRoute("home", "/");

    return router;
}

