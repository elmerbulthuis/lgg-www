import { HttpSendReceive } from "@oas3/http-send-receive";

export interface Settings {
    httpSendReceive?: HttpSendReceive
}
